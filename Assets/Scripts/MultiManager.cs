﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MultiManager : MonoBehaviour {

    [SerializeField]
    private string roomName;

    [SerializeField]
    private string lvlName;

    [SerializeField]
    private bool isHost;

    public static MultiManager instance;

    public string serverInfo;

    [SerializeField]
    private bool roomOK = false;

    [SerializeField]
    private int playerNumber;
    [SerializeField]
    private double secondsToStartGame = 15;

    private DateTime startTime;

    public string[] userNames = new string[4];
    public int[] userPoints = new int[4];

    #region URL

    private string CreateRoomURL = "http://wojtka.home.pl/Jakobiany/CreateRoom.php";
    private string JoinRoomURL = "http://wojtka.home.pl/Jakobiany/JoinRoom.php";
    private string CheckNumberURL = "http://wojtka.home.pl/Jakobiany/CheckPlayersNumber.php";
    private string StartGameURL = "http://wojtka.home.pl/Jakobiany/StartGame.php";
    private string CheckIfStartedURL = "http://wojtka.home.pl/Jakobiany/TEST.php";

    #endregion

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(transform.gameObject);
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    void Start () {
		
	}
	
	void Update () {
		
	}

    public string RoomName
    {
        get
        {
            return roomName;
        }
        set
        {
            roomName = value;
        }
    }

    public string LvlName
    {
        get
        {
            return lvlName;
        }
        set
        {
            lvlName = value;
        }
    }

    public bool IsHost
    {
        get
        {
            return isHost;
        }
        set
        {
            isHost = value;
        }
    }

    public bool RoomOK
    {
        get
        {
            return roomOK;
        }
        set
        {
            roomOK = value;
        }
    }

    public int PlayerNumber
    {
        get
        {
            return playerNumber;
        }
        set
        {
            playerNumber = value;
        }
    }

    public void CreateRoom(string userName, int points)
    {
        StartCoroutine(createRoom(userName, points));
    }

    IEnumerator createRoom(string userName, int points)
    {
        WWWForm form = new WWWForm();
        form.AddField("servernamePOST", roomName);
        form.AddField("namePOST", userName);
        form.AddField("pointsPOST", points);

        WWW www = new WWW(CreateRoomURL, form);

        WaitForSeconds w;
        while (!www.isDone)
            w = new WaitForSeconds(0.1f);

        serverInfo = www.text;
        if(www.text == "Utworzono pokój proawidłowo!")
        {
            RoomOK = true;
        }
        yield return www;
    }

    public void JoinRoom(string userName, int points)
    {
        StartCoroutine(joinRoom(userName, points));
    }

    IEnumerator joinRoom(string userName, int points)
    {
        WWWForm form = new WWWForm();
        form.AddField("servernamePOST", roomName);
        form.AddField("namePOST", userName);
        form.AddField("pointsPOST", points);

        WWW www = new WWW(JoinRoomURL, form);

        WaitForSeconds w;
        while (!www.isDone)
            w = new WaitForSeconds(0.1f);

        serverInfo = www.text;
        if (www.text == "Dodano")
        {
            RoomOK = true;
        }

        Debug.Log(www.text);
        yield return www;
    }

    public void CheckPlayersNumber()
    {
        StartCoroutine(checkPlayersNumber());
    }

    IEnumerator checkPlayersNumber()
    {
        WWWForm form = new WWWForm();
        form.AddField("servernamePOST", roomName);

        WWW www = new WWW(CheckNumberURL, form);

        WaitForSeconds w;
        while (!www.isDone)
            w = new WaitForSeconds(0.1f);

        JsonClass myObject = new JsonClass();


        serverInfo = www.text;

        myObject = JsonUtility.FromJson<JsonClass>(www.text);

        PlayerNumber = myObject.no;

        userNames[0] = myObject.Pl1;
        userNames[1] = myObject.Pl2;
        userNames[2] = myObject.Pl3;
        userNames[3] = myObject.Pl4;

        userPoints[0] = myObject.P1;
        userPoints[1] = myObject.P2;
        userPoints[2] = myObject.P3;
        userPoints[3] = myObject.P4;
        yield return www;
    }

    public class JsonClass
    {
        public int no;
        public string Pl1;
        public string Pl2;
        public string Pl3;
        public string Pl4;

        public int P1;
        public int P2;
        public int P3;
        public int P4;
    }

    public void StartGame()
    {
        StartCoroutine(startGame());
    }

    IEnumerator startGame()
    {
        startTime = DateTime.Now;
        Debug.Log(startTime);
        startTime = startTime.AddSeconds(secondsToStartGame);
        Debug.Log(startTime);

        string formatForMySql = startTime.ToString("yyyy-MM-dd HH:mm:ss");
        
        WWWForm form = new WWWForm();
        form.AddField("servernamePOST", roomName);
        form.AddField("datePOST", formatForMySql);

        WWW www = new WWW(StartGameURL, form);

        WaitForSeconds w;
        while (!www.isDone)
            w = new WaitForSeconds(0.1f);

        yield return www;
    }

    public void CheckStart()
    {
        StartCoroutine(checkStart());
    }

    IEnumerator checkStart()
    {
        WWWForm form = new WWWForm();
        form.AddField("servernamePOST", roomName);

        WWW www = new WWW(CheckIfStartedURL, form);

        WaitForSeconds w;
        while (!www.isDone)
            w = new WaitForSeconds(0.1f);

        serverInfo = www.text;

        yield return www;
    }

}
